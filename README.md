# Description

This Atheros ath9k series WiFi source is from Linux Kernel 4.15 source (Linus Torvald Official Github).

Attention! this is a DKMS module, so it need to install using DKMS.

# Features
- Disable automatic decision to choose regulatory domain (REGDOM).

- Optimized network connection speed endurance for Atheros WiFi.

- Fixed kernel panic kmem_cache_alloc as below.

Call Trace:

 ? kmem_cache_alloc+0xa1/0x1b0
 
 ? __build_skb+0x25/0xe0
 
 ? __netdev_alloc_skb+0xc2/0x110
 
 ? ath9k_cmn_rx_skb_postprocess+0x46/0x130 [ath9k_common]
 
 ? ieee80211_find_sta_by_ifaddr+0xf9/0x190 [mac80211]
 
 ath_rx_tasklet+0xa0f/0xe90 [ath9k]
 
 ath9k_tasklet+0x156/0x230 [ath9k]
 
 tasklet_action+0x5f/0x110
 
 __do_softirq+0xe7/0x2cb
 
 irq_exit+0xf1/0x100
 
 do_IRQ+0x7d/0xc0
 
 common_interrupt+0x87/0x87
 

# Disclaimer
DO NOT USE THIS DRIVER FOR HEAVY TASK, 
I AM NOT RESPONSIBILITY IF YOU HAPPENED SLOW NETWORK CONNECTION CAUSE YOU DOING A HEAVY TASK.

# Changelogs
26/Oct/2018 (Stable Released)
- Removed unstable patches
- Fix frequent flase radar detection

05/Oct/2018
- Fix longer encrypt-decrypt process
- Fix confused channel inspection
- Fix reporting calculated new FFT upper max
- Add back support for using active monitor interfaces for tx99

19/Sep/2018 
- Upgraded net/wext-proc.c api to 4.18
- wext_core.c out of dev_ioctl() use
- Remove return value, cause of void on mesh_pathtbl.c
- Upgraded fix PTK rekey and clear text to v8
- Improve connection-loss timeout
- WPA2-PSK optimized
- WPA-PSK deprecated according security issues
- Ported to kernel 4.18
- End support for kernel 4.15

16/Aug/2018 (End Support for kernel 4.15)
- Removed unused drivers.
- Updated to v6 from v5 fix-PTK-rekey-wlan-freezes-and-cleartext-leaks-v5.patch
- mac80211_Define-new-driver-callback-replace_key.patch

07/Aug/2018
- More stable connection, Thanks to fix-PTK-rekey-wlan-freezes-and-cleartext-leaks-v5.patch.
- mac80211_Define-new-driver-callback-replace_key.patch.
- mac80211_ignore-SA-Query-requests-with-unknown-payload-data.patch

26/Jul/2018
- New patch from ath.git kvalo including : 
    - Clear potentially stale EOSP status bit in intermediate queues.
    - Don't run periodic and nf calibation at the same time.
    - And more, please see patches folder.

19/Jul/2018
- Use native encryption/decryption algorhythm for lib80211-tkip

10/Jul/2018
- Added stop start logic for software TXQ's.
- Restrict delayed tailroom needed decrement.
- Improved better network connection.

05/Jul/2018
- Added patch file for mac80211 and ath9k-htc to support hw does not support QOS NDP.
- Added header file mac80211.h to be included it self so don't use system mac80211.h.
- Fix PTK rekey for prevent wireless freezes and cleartext leaks.

02/Jul/2018
- Added patch from upstream, gregkh and others.

# Installation
1. Clone or download this repo, if you download it, you must extract first.
2. Add it to DKMS as below

   <code>sudo dkms add ./Atheros-ath9k-wifi-fix-((version / master))/</code>
3. Install it using DKMS as below

   <code>sudo dkms install wifi-ath9k-fix/((version)) --force</code>
4. Don't forget to add your country regulatory domain (for example REGDOM=US COUNTRY=US) on crda.
5. Reboot or restart your GNU/Linux machine to check are your Atheros ath9k WiFI is functioned.

# Uninstall
1. Uninstall it using DKMS as below

   <code>sudo dkms remove wifi-ath9k-fix/((version)) --all</code>
4. Reboot or restart your GNU/Linux machine.
